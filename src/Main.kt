fun main() {

    val p1 = Point(11,13)
    val p2 = Point(15,18)
    val k =  p1 +p2

    println(k)
    println(-k)

    val list = mutableListOf(4,5,6)
    println(list)
    list.swap(0,2)
    println(list)

    println(list.lastIndex)


}

data class Point(val x: Int, val y: Int) {

    operator fun plus(point: Point) :Point {
        return Point(this.x + point.x, this.y + point.y)
    }

}


operator fun Point.unaryMinus() = Point (-x, -y)

fun <E> MutableList<E>.swap(firstIndex: Int, secondIndex: Int) {
    val temp = this[firstIndex]
    this[firstIndex] = this[secondIndex]
    this[secondIndex] = temp
}


val <T> List<T>.lastIndex: Int
    get() = this.size - 1